'use strict';

const Nodal = require('nodal');

class CreateWorkers extends Nodal.Migration {

  constructor(db) {
    super(db);
    this.id = 2017032122303669;
  }

  up() {

    return [
      this.createTable("workers", [{"name":"user_id","type":"int"},{"name":"hourly_rate","type":"int"},{"name":"cover","type":"string"},{"name":"about","type":"string"},{"name":"category","type":"string"},{"name":"lat","type":"string"},{"name":"lng","type":"string"},{"name":"visible","type":"boolean"}])
    ];

  }

  down() {

    return [
      this.dropTable("workers")
    ];

  }

}

module.exports = CreateWorkers;
