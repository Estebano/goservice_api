'use strict';

const Nodal = require('nodal');

class CreateConversations extends Nodal.Migration {

  constructor(db) {
    super(db);
    this.id = 2017041200144120;
  }

  up() {

    return [
      this.createTable("conversations", [{"name":"user_id","type":"int"},{"name":"recipient_id","type":"int", "properties":{"unique":true}},{"name":"recipient_name_first","type":"string"},{"name":"recipient_name_last","type":"string"},{"name":"recipient_picture","type":"string"}])
    ];

  }

  down() {

    return [
      this.dropTable("conversations")
    ];

  }

}

module.exports = CreateConversations;
