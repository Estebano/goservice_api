'use strict';

const Nodal = require('nodal');

class CreateAds extends Nodal.Migration {

  constructor(db) {
    super(db);
    this.id = 2017032801565655;
  }

  up() {

    return [
      this.createTable("ads", [{"name":"user_id","type":"int"},{"name":"title","type":"string"},{"name":"category","type":"string"},{"name":"description","type":"string"},{"name":"picture","type":"string"}])
    ];

  }

  down() {

    return [
      this.dropTable("ads")
    ];

  }

}

module.exports = CreateAds;
