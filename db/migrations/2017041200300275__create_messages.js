'use strict';

const Nodal = require('nodal');

class CreateMessages extends Nodal.Migration {

  constructor(db) {
    super(db);
    this.id = 2017041200300275;
  }

  up() {

    return [
      this.createTable("messages", [{"name":"sender_id","type":"int"},{"name":"recipient_id","type":"int"},{"name":"message","type":"string"},{"name":"is_new","type":"boolean"}])
    ];

  }

  down() {

    return [
      this.dropTable("messages")
    ];

  }

}

module.exports = CreateMessages;
