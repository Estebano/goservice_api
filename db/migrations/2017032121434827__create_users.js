'use strict';

const Nodal = require('nodal');

class CreateUsers extends Nodal.Migration {

  constructor(db) {
    super(db);
    this.id = 2017032121434827;
  }

  up() {

    return [
      this.createTable("users", [{"name":"email","type":"string","properties":{"unique":true}},{"name":"password","type":"string"},{"name":"username","type":"string"}, {"name":"name_first","type":"string"},{"name":"name_last","type":"string"},{"name":"picture","type":"string"},{"name":"rate","type":"int"},{"name":"state","type":"string"},{"name":"postal_code","type":"int"},{"name":"worker","type":"boolean"}])
    ];

  }

  down() {

    return [
      this.dropTable("users")
    ];

  }

}

module.exports = CreateUsers;
