'use strict';

const Nodal = require('nodal');

class CreateNotifications extends Nodal.Migration {

  constructor(db) {
    super(db);
    this.id = 2017041323061300;
  }

  up() {

    return [
      this.createTable("notifications", [{"name":"user_id","type":"int"},{"name":"recipient_id","type":"int"},{"name":"new_bookings","type":"int"},{"name":"new_messages","type":"int"}])
    ];

  }

  down() {

    return [
      this.dropTable("notifications")
    ];

  }

}

module.exports = CreateNotifications;
