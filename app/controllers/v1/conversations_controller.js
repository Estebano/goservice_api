'use strict';

const Nodal = require('nodal');
const Conversation = Nodal.require('app/models/conversation.js');

class V1ConversationsController extends Nodal.Controller {

  index() {

    Conversation.query()
      .where(this.params.query)
      .orderBy('created_at', 'DESC')
      .end((err, models) => {

        this.respond(err || models);

      });

  }

  show() {

    Conversation.find(this.params.route.id, (err, model) => {

      this.respond(err || model);

    });

  }

  create() {

    Conversation.create(this.params.body, (err, model) => {

      this.respond(err || model);

    });

  }

  update() {

    Conversation.update(this.params.route.id, this.params.body, (err, model) => {

      this.respond(err || model);

    });

  }

  destroy() {

    Conversation.destroy(this.params.route.id, (err, model) => {

      this.respond(err || model);

    });

  }

}

module.exports = V1ConversationsController;
