'use strict';

const Nodal = require('nodal');
const Worker = Nodal.require('app/models/worker.js');

class V1WorkersController extends Nodal.Controller {

  index() {

    Worker.query()
        .join('user')
      .where(this.params.query)
      .end((err, models) => {

        this.respond(err || models, ['id', 'user_id','hourly_rate','cover','about','category','visible','lat','lng','created_at', {user: [ 'id',
                                                                                                                                          'username',
                                                                                                                                          'email',
                                                                                                                                          'name_first',
                                                                                                                                          'name_last',
                                                                                                                                          'picture',
                                                                                                                                          'rate',
                                                                                                                                          'worker',
                                                                                                                                          'state',
                                                                                                                                          'postal_code',
                                                                                                                                          'created_at']}
        ]);

      });

  }

//['id', 'body','created_at', {user: ['id', 'username', 'created_at']}]

  show() {

    Worker.find(this.params.route.id, (err, model) => {

      this.respond(err || model);

    });

  }

  create() {

    Worker.create(this.params.body, (err, model) => {

      this.respond(err || model);

    });

  }

  update() {

    Worker.update(this.params.route.id, this.params.body, (err, model) => {

      this.respond(err || model);

    });

  }

  destroy() {

    Worker.destroy(this.params.route.id, (err, model) => {

      this.respond(err || model);

    });

  }

}

module.exports = V1WorkersController;
