'use strict';

const Nodal = require('nodal');
const Ad = Nodal.require('app/models/ad.js');

class V1AdsController extends Nodal.Controller {

  index() {

    Ad.query()
        .join('user')
      .where(this.params.query)
        .orderBy('created_at', 'DESC')
      .end((err, models) => {

        this.respond(err || models, ['id', 'user_id','title','category','description','picture','created_at',{user: [ 'id',
                                                                                                            'username',
                                                                                                            'email',
                                                                                                            'name_first',
                                                                                                            'name_last',
                                                                                                            'picture',
                                                                                                            'rate',
                                                                                                            'worker',
                                                                                                            'state',
                                                                                                            'postal_code',
                                                                                                            'created_at']}
        ]);

      });

  }

  show() {

    Ad.find(this.params.route.id, (err, model) => {

      this.respond(err || model);

    });

  }

  create() {

    Ad.create(this.params.body, (err, model) => {

      this.respond(err || model);

    });

  }

  update() {

    Ad.update(this.params.route.id, this.params.body, (err, model) => {

      this.respond(err || model);

    });

  }

  destroy() {

    Ad.destroy(this.params.route.id, (err, model) => {

      this.respond(err || model);

    });

  }

}

module.exports = V1AdsController;
