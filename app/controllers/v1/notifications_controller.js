'use strict';

const Nodal = require('nodal');
const Notification = Nodal.require('app/models/notification.js');

class V1NotificationsController extends Nodal.Controller {

  index() {

    Notification.query()
      .join('user')
      .where(this.params.query)
      .end((err, models) => {
        this.respond(err || models,['id', 'user_id','recipient_id','new_bookings','new_messages','created_at','updated_at', {user: [ 'id',
                                                                                                                                    'username',
                                                                                                                                    'email',
                                                                                                                                    'name_first',
                                                                                                                                    'name_last',
                                                                                                                                    'picture',
                                                                                                                                    'rate',
                                                                                                                                    'worker',
                                                                                                                                    'state',
                                                                                                                                    'postal_code',
                                                                                                                                    'created_at']}
        ]);
      });

  }

  show() {

    Notification.find(this.params.route.id, (err, model) => {

      this.respond(err || model);

    });

  }

  create() {

    Notification.create(this.params.body, (err, model) => {

      this.respond(err || model);

    });

  }

  update() {

    Notification.update(this.params.route.id, this.params.body, (err, model) => {

      this.respond(err || model);

    });

  }

  destroy() {

    Notification.destroy(this.params.route.id, (err, model) => {

      this.respond(err || model);

    });

  }

}

module.exports = V1NotificationsController;
