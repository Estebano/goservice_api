'use strict';

const Nodal = require('nodal');
const User = Nodal.require('app/models/user.js');

class Ad extends Nodal.Model {}

Ad.setDatabase(Nodal.require('db/main.js'));
Ad.setSchema(Nodal.my.Schema.models.Ad);

Ad.joinsTo(User, {multiple:true});

module.exports = Ad;
