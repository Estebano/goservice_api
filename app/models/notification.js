'use strict';

const Nodal = require('nodal');
const User = Nodal.require('app/models/user.js');

class Notification extends Nodal.Model {}

Notification.setDatabase(Nodal.require('db/main.js'));
Notification.setSchema(Nodal.my.Schema.models.Notification);
Notification.joinsTo(User, {multiple:true});

module.exports = Notification;
