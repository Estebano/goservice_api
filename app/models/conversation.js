'use strict';

const Nodal = require('nodal');

class Conversation extends Nodal.Model {}

Conversation.setDatabase(Nodal.require('db/main.js'));
Conversation.setSchema(Nodal.my.Schema.models.Conversation);

module.exports = Conversation;
