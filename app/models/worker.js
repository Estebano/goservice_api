'use strict';

const Nodal = require('nodal');
const User = Nodal.require('app/models/user.js');

class Worker extends Nodal.Model {}



Worker.setDatabase(Nodal.require('db/main.js'));
Worker.setSchema(Nodal.my.Schema.models.Worker);

Worker.joinsTo(User, {multiple:true});

module.exports = Worker;
